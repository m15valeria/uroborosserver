package client;

import java.io.IOException;
import java.net.HttpURLConnection;

public class Client {

    public static void main(String[] args) throws IOException {
        startClient();
    }

    /**
     * Method: Starts all client methods.
     *
     * @throws IOException
     */
    public static void startClient() throws IOException {
        Console console = new Console();
        //Get the schedule parameters from the user
        int[] chooseData = console.chooseMode();
        int list = console.getList();
        
        ServerInteraction serverInteraction = new ServerInteraction();
        //Generates data for the address and assembly it
        String[] addressData = console.inputAddressData(chooseData[0], chooseData[1], chooseData[2]);
        String address = serverInteraction.addressAssembly(addressData, list, chooseData[2]);

        String content = null;
        try {
            //Connecting to the server and sending a request to get
            HttpURLConnection connect = serverInteraction.сreateСonnection(address);
            if(chooseData[2]==1){
                connect = serverInteraction.createGetRequest(connect);
                content = serverInteraction.sendGetRequest(connect);
                //Parsing content
                String[] table = new TableFormatter().createPrintableStringFromJson(content);
                //Formatted output schedule
                console.outputData(table); 
            }
            else{
                serverInteraction.sendPOSTRequest(connect, addressData[1]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
