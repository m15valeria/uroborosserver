package client;

import java.util.Scanner;

public class Console {

    /**
     * Field list.
     */
    static int list = 0;
    /**
     * Format for output of a schedule table.
     */
    private static final String tableFormat = "%-20s|%-20s|%-20s|%-20s|%-60s|%-20s|%-12s|%-60s|%-8s|%-8s\n";
    /**
     * Format for оutput of groups of a certain professor.
     */
    private static final String professorsFormat = "%-12s|%-60s\n";

    /**
     * Method: Gets the value of the list field.
     *
     * @return value of the list field.
     */
    public static int getList() {
        return list;
    }

    /**
     * Method: In the format of a dialog with the user, he learns by what
     * parameters he needs to get a schedule.To begin with, the user selects 
     * the type of request to be sent to the serverIf the Professor parameter is
     * selected, it asks the user whether to display a list of groups of a
     * certain professor or not.If the answer is "No", it asks if the user is
     * interested in the schedule for a specific day of the week.If the Exit
     * parameter is selected, the program shuts down.
     *
     * @return array with data selected by the user.
     */
    public static int[] chooseMode() {

        Scanner in = new Scanner(System.in);
        int method;
        //Chosen parameter
        int param;
        //Day of the week
        int day = 0;
        System.out.print("Which method do you want to work with?:\n"
                            + "1.GET\n"
                            + "2.POST\n"
                            + "Choose one:");
        method=in.nextInt();
        /* Output of the parameter selection menu*/
        System.out.print(
                "What table do you want to work with?:\n"
                + "1. Group;\n"
                + "2. Professor;\n"
                + "3. Auditorium;\n"
                + "4. Exit");

        do {
            System.out.print("\nChoose one:");
            param = in.nextInt();
            if (param < 1 || param > 4) {
                System.out.println("Please, choose correct value");
            }
        } while (param < 1 || param > 4);

        /* If the 4nd(Exit) menu item is selected */
        if (param == 4) {
            System.exit(0);
        }

        /* If the 2nd(Professor) menu item is selected */
        if (param == 2 && method==1) {
            System.out.print(
                    "Are you interested in list of groups in which a certain professor leads?\n"
                    + "1. Yes;\n"
                    + "2. No.");
            do {
                System.out.print("\nChoose one:");
                list = in.nextInt();
                if (list < 0 || list > 2) {
                    System.out.println("Please, choose correct value");
                }
            } while (list < 1 || list > 2);
        }

        /* If the user is NOT interested in the list of groups of a certain professor */
        if (list != 1 && method==1) {
            System.out.print(
                    "Are you interested in the schedule for a particular day?\n"
                    + "1. Yes;\n"
                    + "2. No.");
            do {
                System.out.print("\nChoose one:");
                day = in.nextInt();
                if (day < 0 || day > 2) {
                    System.out.println("Please, choose correct value");
                }
            } while (day < 1 || day > 2);
        }

        return new int[]{param, day, method};
    }

    /**
     * Method: In the dialog format, depending on the parameter previously
     * selected by the user, generates data for the address assembly.Depending 
     * on which request is sent to the server if the user is interested in a 
     * certain day of the week and is not interested in the list of groups of 
     * a certain professor asks the user for the name of the day of the week.
     *
     * @param param - The parameter selected by the user.
     * @param day - Specific day of the week (1-Yes, 2-No).
     * @param method - request method (1-GET,2-POST).
     * @return array with data for address assembly
     */
    public static String[] inputAddressData(int param, int day, int method) {
        Scanner in = new Scanner(System.in);
        /* index: 0-class; 1-Entity; 2-day of the week. */
        String[] addressData = new String[]{null, null, null};
        //If request method is GET
        if(method==1)
        switch (param) {
            /* If the Group parameter is chosen */
            case 1: {
                addressData[0] = "Group";
                System.out.print("Enter the name of the group whose schedule you are looking for:");
                addressData[1] = in.nextLine();
                break;
            }

            /* If the Professor parameter is chosen */
            case 2: {
                addressData[0] = "Professor";
                System.out.print("Enter the name of the professor whose schedule you are looking for:");
                addressData[1] = in.nextLine();
                break;
            }

            /* If the  Auditorium  parameter is chosen */
            case 3: {
                addressData[0] = "Auditorium";
                System.out.print("Enter the name of the auditorium whose schedule you are looking for(Example - D-508):");
                addressData[1] = in.nextLine();
                break;
            }
        }
        else //If request method is POST
           switch (param){
           case 1: {
                addressData[0] = "Group";
                break;
            }

            /* If the Professor parameter is chosen */
            case 2: {
                addressData[0] = "Professor";
                break;
            }

            /* If the  Auditorium  parameter is chosen */
            case 3: {
                addressData[0] = "Auditorium";
                System.out.print("Enter the path to the .json file from which you want to add data to the table 'Auditorium':");
                addressData[1]=in.nextLine();
                break;
            } 
        }
        /* if a specific day is needed */
        if (list != 1 && day != 2 && method==1) {
            System.out.print("\nEnter day names:");
            addressData[2] = in.nextLine();
        }

        return addressData;
    }

    /**
     * Method: Parses and formatted outputs the schedule to the console,
     * depending on whether the user is interested in a list of groups of a
     * certain professor or not.
     *
     * @param schedule - table with the schedule to be output.
     */
    public static void outputData(String[] schedule) {
        if (!"".equals(schedule[0])) {
            /* Data output depending on the list mode */
            if (list != 1) {
                System.out.printf(tableFormat, "Day of the week", "Lesson start time", "Lesson end time", "Parity of week", "Subject name", "Lesson type", "Group name", "Professor", "Housing", "Cabinet");
                for (String s : schedule) {
                    String[] temp = s.split("_");
                    System.out.printf(tableFormat, temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7], temp[8], temp[9]);
                }
                System.out.println();
            } else {
                System.out.printf(professorsFormat, "Group", "Professor");
                for (String s : schedule) {
                    String[] temp = s.split("_");
                    System.out.printf(professorsFormat, temp[0], temp[1]);
                }
                System.out.println();
            }
        } else {
            System.out.println("The schedule table is empty!");
        }
    }

}
