from app.core.serve import UroborosServer
from configparser import ConfigParser
from http.server import HTTPServer

# Creating a ConfigParser object to read the configuration file
config = ConfigParser()


def runServer():
    """Method for running server.

    Behaviour:
        Having previously read the config to get the port and host,
     creates and starts the server using the HttpServer class.

    Exceptions:
        Stops the server if the user presses the process interrupt key.
    """
    # Reading the config
    config.read("config/config.ini")
    host_name = config.get("server", "host_name")
    serverPort = int(config.get("server", "serverPort"))
    print(host_name, serverPort)
    # Creating and running a server
    webServer = HTTPServer((host_name, serverPort), UroborosServer)
    print("Server started http://%s:%s" % (host_name, serverPort))
    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass
    # Stopping the server in case of an KeyboardInterrupt exception
    webServer.server_close()
    print("Server stopped.")


if __name__ == '__main__':
    runServer()
