from ..controllers import Group, Professor, Auditorium


class Router(object):
    """Router class is used for performing request routing.

    Attributes are
     * prof: Professor's name;
     * group: Group name;
     * auditor: Auditorium name;
     * controllers: Dictionary of controllers.

    Methods:
        __init__():
            Class constructor;
        __check_controllers(controller):
            Checks controllers;
        get_all(controller, entity):
            Gets all controller entities;
        get_day(controller: str, entity: str, day: str):
            Gets entity data by day of the week;
        get_groups(controller, entity, group):
            Gets entity data by groups;
        add(controller: str, entity: dict):
            Adds an entity;
        edit(entity):
            Edit an entity;
        delete(entity):
            Deleteы an entity.
    """

    def __init__(self):
        """Method (class constructor) for initializing attributes.

        Behaviour:
            Initializes data: prof, group, auditor, controllers.
        """

        self.prof = Professor.Professor()
        self.group = Group.Group()
        self.auditor = Auditorium.Auditorium()
        self.controllers = {
            "Professor": self.prof,
            "Group": self.group,
            "Auditorium": self.auditor
        }

    def __check_controllers(self, controller):
        """Method for checking controllers.

        Behaviour:
            Checks the dictionary of controllers for the presence of
         the received controller.

        Attributes are
         * controller: Controller name;

        Returns:
         * True or False: Check result;
        """

        return controller in self.controllers

    def get_all(self, controller, entity):
        """Method for getting all controller entities.

        Behaviour:
            Accesses the dictionary of controllers and activates the get_all method.

        Attributes are
         * controller: Controller name;
         * entity: Entity.

        Returns:
         * The result of executing the get_all method of the specified controller;
         * False.
        """

        # If the controller has passed
        if self.__check_controllers(controller):
            # Returning the result of the get_all() method
            return self.controllers[controller].get_all(entity)
        else:
            return False

    def get_day(self, controller: str, entity: str, day: str):
        """Method for getting entity data by day of the week.

        Behaviour:
            Accesses the dictionary of controllers and activates the get_day method.

        Attributes are
         * controller: Controller name;
         * entity: Entity;
         * day: Day of the week.

        Returns:
         * The result of executing the get_day method of the specified controller;
         * False.
        """

        # If the controller has passed
        if self.__check_controllers(controller):
            # Returning the result of the get_day() method
            return self.controllers[controller].get_day(entity, day)
        else:
            return False

    def get_groups(self, controller, entity, group):
        """Method for getting entity data by groups.

        Behaviour:
            Accesses the dictionary of controllers and activates the get_group method.

        Attributes are
         * controller: Controller name;
         * entity: Entity;
         * group: Group name.

        Returns:
         * The result of executing the get_group method of the specified controller;
         * False.
        """

        # If the controller has passed
        if self.__check_controllers(controller):
            # Returning the result of the get_group() method
            return self.controllers[controller].get_group(entity, group)
        else:
            return False

    def add(self, controller: str, entity: dict):
        """Method for adding an entity.

        Behaviour:
            Accesses the dictionary of controllers and activates the add method.

        Attributes are
         * controller: Controller name;
         * entity: Entity.

        Returns:
         * The result of executing the add method of the specified controller.
        """
        # Running the add() method
        self.controllers[controller].add(entity)

    def edit(self, entity, ):
        """Method for editing an entity.

        Attributes are
         * entity: Entity.
        """

        pass

    def delete(self, entity):
        """Method for deleting an entity.

        Attributes are
         * entity: Entity.
        """

        pass
